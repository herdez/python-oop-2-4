# Object Initialization - Exercise

## Initializing Instance Objects

Define `Driver` class. Initialize a driver with the attributes `first`, `last`, and `occupation` for their first name, last name, and occupation. Provide a default argument of `"driver"` for `occupation`.


```python
# Define Driver class here

```


```python
# Initialize Gary White here and pass tests

print(gary_white.first == "Gary") 
print(gary_white.last == "White")
print(gary_white.occupation == "Astronaut") 
```
```
    True
    True
    True
``` 

Initialize all instances contain the attributes `first`, `last`, `email`, and `rides_taken` for their first name, last name, email, and number of rides they have taken. Provide a default argument of `0` for the `rides_taken` attribute since new passengers should not have taken any rides. 


```python
# Define Passenger class here

```


```python
# Initialize Mr. Standley here and tests must be True


print(carlos.first == "Carlos") 
print(carlos.last == "Standley") 
print(carlos.email == "carlos.stand@mailimax.com") 
print(carlos.rides_taken == 0) 
```
``` 
    True
    True
    True
    True
```


```python
# Initialize Mr. Moyu here and tests must be True


print(moyu.first == "Yakohama") 
print(moyu.last == "Moyu") 
print(moyu.email == "yaku.yu@mailimax.com") 
print(moyu.rides_taken == 0) 
```
``` 
    True
    True
    True
    True
```  


```python
# tests must be True 

carlos = Passenger(rides_taken = 7)
print(carlos.first == "Carlos") 
print(carlos.last == "Standley") 
print(carlos.email == "carlos.stand@mailimax.com") 
print(carlos.rides_taken == 7) 
```
``` 
    True
    True
    True
    True
``` 

